package xyz.nzt48.covidnews.context

import org.springframework.util.StringUtils
import xyz.nzt48.covidnews.enums.Languages

class LanguageContext {
    companion object {
        private val threaded : ThreadLocal<Languages> = ThreadLocal()

        fun setLanguage(code : String?, default : Languages){
            if(StringUtils.isEmpty(code)){
                threaded.set(default)
            }
            else {
                threaded.set(Languages.valueOf(code!!.replace(Regex("\\W"), "_").toUpperCase()))
            }
        }

        fun getLanguage() : Languages {
            return threaded.get();
        }
    }
}