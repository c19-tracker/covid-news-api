package xyz.nzt48.covidnews.filters

import org.springframework.beans.factory.annotation.Value
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import xyz.nzt48.covidnews.context.LanguageContext
import xyz.nzt48.covidnews.enums.Languages
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@Order
class LanguageFilter : OncePerRequestFilter() {

    @Value("\${xyz.nzt48.covid-news.default-language}")
    var defaultLanuage : String? = null;

    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, next: FilterChain) {
        LanguageContext.setLanguage(req.getHeader("Language"), Languages.valueOf(defaultLanuage.orEmpty()))
        next.doFilter(req, res);
    }
}