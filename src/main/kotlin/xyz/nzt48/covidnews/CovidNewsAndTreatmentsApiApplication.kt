package xyz.nzt48.covidnews

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories(basePackages = ["xyz.nzt48.covidnews.repos"])
class CovidNewsAndTreatmentsApiApplication

fun main(args: Array<String>) {
	runApplication<CovidNewsAndTreatmentsApiApplication>(*args)
}
