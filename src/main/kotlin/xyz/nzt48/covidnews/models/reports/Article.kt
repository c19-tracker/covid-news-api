package xyz.nzt48.covidnews.models.reports

import xyz.nzt48.covidnews.enums.Emotion
import xyz.nzt48.covidnews.enums.Languages
import xyz.nzt48.covidnews.enums.Purpose
import xyz.nzt48.covidnews.models.base.Base
import xyz.nzt48.covidnews.models.fields.Fields
import xyz.nzt48.covidnews.models.fields.TableNames
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = TableNames.ARTICLE)
class Article : Base() {

    @Column(name = Fields.LANGUAGE, nullable = false)
    var language : Languages? = null;

    @Column(name = Fields.PURPOSE, nullable = false)
    var purpose : Purpose? = null;

    @Column(name = Fields.EMOTION, nullable = false)
    var emotion : Emotion? = null;

    @Column(name = Fields.URL, nullable = false)
    var url : String? = null;

    @Column(name = Fields.TITLE, nullable = false)
    var title : String? = null;

}