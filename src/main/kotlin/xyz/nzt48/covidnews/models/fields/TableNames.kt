package xyz.nzt48.covidnews.models.fields

class TableNames {
    companion object {
        const val ARTICLE = "article"
        const val TREATMENT = "treatment"
        const val TREATMENT_TITLE = "treatment_title"
        const val TREATMENT_BODY = "treatment_body"
    }
}