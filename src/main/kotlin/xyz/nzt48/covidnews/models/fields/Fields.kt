package xyz.nzt48.covidnews.models.fields

class Fields {
    companion object {
        const val ID = "id"
        const val CREATED_AT = "created_at"
        const val CREATED_BY = "created_by"
        const val UPDATED_AT = "updated_at"
        const val UPDATED_BY = "updated_by"

        const val LANGUAGE = "language"
        const val FIELD = "field"

        const val PURPOSE = "purpose"
        const val EMOTION = "emotion"
        const val URL = "url"
        const val TITLE = "title"

        const val PHASE = "phase"

        const val TREATMENT = "TREATMENT_ID"
        const val HTML = "HTML"
    }
}