package xyz.nzt48.covidnews.models.treatments

import com.fasterxml.jackson.annotation.JsonIgnore
import xyz.nzt48.covidnews.models.base.LanguageField
import xyz.nzt48.covidnews.models.fields.Fields
import xyz.nzt48.covidnews.models.fields.TableNames
import javax.persistence.*

@Entity
@Table(name = TableNames.TREATMENT_TITLE, uniqueConstraints = [ UniqueConstraint( columnNames = [ Fields.TREATMENT, Fields.LANGUAGE ] ) ] )
class TreatmentTitle : LanguageField() {
    @JsonIgnore
    @JoinColumn(name = Fields.TREATMENT, nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    var treatment : Treatment? = null;
}