package xyz.nzt48.covidnews.models.treatments

import xyz.nzt48.covidnews.enums.StudyPhase
import xyz.nzt48.covidnews.models.base.Base
import xyz.nzt48.covidnews.models.fields.Fields
import xyz.nzt48.covidnews.models.fields.TableNames
import javax.persistence.*

@Entity
@Table(name = TableNames.TREATMENT)
class Treatment : Base() {

    @Column(name = Fields.PHASE)
    var phase : StudyPhase? = null;

    @OneToMany(cascade = [CascadeType.REMOVE], mappedBy = "treatment")
    var bodies : List<TreatmentBody>? = null

    @OneToMany(cascade = [CascadeType.REMOVE], mappedBy = "treatment")
    var titles : List<TreatmentTitle>? = null

}