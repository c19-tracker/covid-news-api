package xyz.nzt48.covidnews.models.base

import xyz.nzt48.covidnews.enums.Languages
import xyz.nzt48.covidnews.models.fields.Fields
import javax.persistence.Column
import javax.persistence.MappedSuperclass

@MappedSuperclass
open class LanguageField : Base() {

    @Column(name = Fields.LANGUAGE, nullable = false)
    var language : Languages? = null

    @Column(name = Fields.FIELD, nullable = false)
    var field : String? = null

}