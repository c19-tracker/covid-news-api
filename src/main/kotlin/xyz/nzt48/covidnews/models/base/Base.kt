package xyz.nzt48.covidnews.models.base

import xyz.nzt48.covidnews.models.fields.Fields
import java.util.*
import javax.persistence.*

@MappedSuperclass
open class Base {

    @Id()
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = Fields.ID, updatable = false, unique = true, nullable = false)
    var id : Long? = null;

    @Column(name = Fields.CREATED_AT, updatable = false, nullable = false)
    var createdAt : Date? = null;

    @Column(name = Fields.CREATED_BY, updatable = false)
    var createdBy : String? = null;

    @Column(name = Fields.UPDATED_AT)
    var updatedAt : Date? = null;

    @Column(name = Fields.UPDATED_BY)
    var updatedBy : String? = null;

}