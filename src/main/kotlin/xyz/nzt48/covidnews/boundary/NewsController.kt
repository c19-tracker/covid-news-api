package xyz.nzt48.covidnews.boundary

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*
import xyz.nzt48.covidnews.dto.SearchDto
import xyz.nzt48.covidnews.models.reports.Article
import xyz.nzt48.covidnews.services.ArticleService

@RestController
@RequestMapping("/news")
class NewsController @Autowired constructor(val service: ArticleService){

    @PostMapping("/search")
    fun currentLanguage(@RequestBody(required = true) body : SearchDto<Article>) : Page<Article> {
        return service.examplePaged(body.example, body.page.toPageable());
    }

    @PostMapping()
    fun save(@RequestBody article: Article) : Article{
        return service.save(article);
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable(name = "id") id : Long) : Article? {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable(name = "id") id : Long) {
        service.delete(service.getById(id)!!);
    }

}