package xyz.nzt48.covidnews.boundary

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import xyz.nzt48.covidnews.enums.Languages

@RestController
@RequestMapping("/")
class IndexController {

    @GetMapping
    fun hello() : Map<String, String> {
        return mapOf()
    }

    @GetMapping("/languages/field-value")
    fun availableLanguages() : List<String> {
        return Languages.values().map { it.name }
    }

    @GetMapping("/languages/code")
    fun availableLanguagesCodes() : List<String> {
        return Languages.values().map { it.getLanguageCode() }
    }
}






