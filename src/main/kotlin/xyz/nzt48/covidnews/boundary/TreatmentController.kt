package xyz.nzt48.covidnews.boundary

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import xyz.nzt48.covidnews.dto.TreatmentDto
import xyz.nzt48.covidnews.dto.TreatmentIdTitleDto
import xyz.nzt48.covidnews.models.treatments.Treatment
import xyz.nzt48.covidnews.services.TreatmentService

@RestController
@RequestMapping("/treatments")
class TreatmentController @Autowired constructor(var service : TreatmentService) {

    @PostMapping
    fun saveUpdate(@RequestBody treatmentDto : TreatmentDto) : TreatmentDto {
        return service.save(treatmentDto)
    }

    @GetMapping("/{id}")
    fun getTreatment(@PathVariable( value = "id" ) id : Long) : TreatmentDto {
        return service.getById(id)
    }

    @GetMapping("/{id}/raw")
    fun getRawTreatment(@PathVariable( value = "id" ) id : Long) : Treatment {
        return service.rawTreatment(id)
    }

    @GetMapping(value = ["/{id}/body"], produces = ["text/html"])
    fun getHtml(@PathVariable( value = "id" ) id : Long) : String {
        return service.getHtml(id)
    }

    @GetMapping
    fun getAllTreatments() : List<TreatmentIdTitleDto> {
        return service.allAvailableTreatments()
    }

    @DeleteMapping("/{id}")
    fun deleteById(id : Long) {
        return service.delete(id);
    }

}