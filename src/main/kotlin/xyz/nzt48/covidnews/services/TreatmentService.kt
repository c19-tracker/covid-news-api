package xyz.nzt48.covidnews.services

import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import xyz.nzt48.covidnews.context.LanguageContext
import xyz.nzt48.covidnews.dto.TreatmentDto
import xyz.nzt48.covidnews.dto.TreatmentIdTitleDto
import xyz.nzt48.covidnews.enums.Languages
import xyz.nzt48.covidnews.models.treatments.Treatment
import xyz.nzt48.covidnews.models.treatments.TreatmentBody
import xyz.nzt48.covidnews.models.treatments.TreatmentTitle
import xyz.nzt48.covidnews.repos.TreatmentBodyRepo
import xyz.nzt48.covidnews.repos.TreatmentRepo
import xyz.nzt48.covidnews.repos.TreatmentTitleRepo
import xyz.nzt48.covidnews.services.abstractions.Crud
import xyz.nzt48.covidnews.utils.hibernateUnproxy

@Service
class TreatmentService @Autowired constructor(final val repo: TreatmentRepo,
                                              final val titleRepo: TreatmentTitleRepo,
                                              final val bodyRepo: TreatmentBodyRepo){

    var treatmentCrud : Crud<Treatment, TreatmentRepo> = Crud(repo);
    var titleCrud : Crud<TreatmentTitle, TreatmentTitleRepo> = Crud(titleRepo);
    var bodyCrud : Crud<TreatmentBody, TreatmentBodyRepo> = Crud(bodyRepo);

    var parser = Parser.builder().build();
    var renderer = HtmlRenderer.builder().build();

    fun getById(id : Long) : TreatmentDto {
        var treatment = repo.getOne(id);

        var output = TreatmentDto()

        output.id = treatment.id
        output.language = LanguageContext.getLanguage()
        output.body = bodyRepo.findByTreatmentAndLanguage(treatment, LanguageContext.getLanguage()).orElse(TreatmentBody()).field
        output.title = titleRepo.findByTreatmentAndLanguage(treatment, LanguageContext.getLanguage()).orElse(TreatmentTitle()).field
        output.phase = treatment.phase

        return output;
    }

    @Transactional
    fun save(treatmentDto: TreatmentDto) : TreatmentDto{

        val activeLanguages : Languages = treatmentDto.language ?: LanguageContext.getLanguage()

        var html = renderer.render(parser.parse(treatmentDto.body));

        val treatment : Treatment = repo.findById(treatmentDto.id ?: 0L).orElse(Treatment());

        treatment.phase = treatmentDto.phase

        var treatmentTitle : TreatmentTitle = TreatmentTitle();
        if (treatment.id != null) {
            treatmentTitle = titleRepo
                    .findByTreatmentAndLanguage(treatment, activeLanguages)
                    .orElse(TreatmentTitle())
        }

        treatmentTitle.treatment = treatment
        treatmentTitle.field = treatmentDto.title!!
        treatmentTitle.language = activeLanguages

        var treatmentBody : TreatmentBody = TreatmentBody()
        if (treatment.id != null) {
            treatmentBody = bodyRepo
                    .findByTreatmentAndLanguage(treatment, activeLanguages)
                    .orElse(TreatmentBody())
        }

        treatmentBody.treatment = treatment
        treatmentBody.field = treatmentDto.body
        treatmentBody.language = activeLanguages
        treatmentBody.html = html

        // TODO: process field into html

        treatmentCrud.save(treatment)
        titleCrud.save(treatmentTitle)
        bodyCrud.save(treatmentBody)

        return treatmentDto
    }

    fun allAvailableTreatments() : List<TreatmentIdTitleDto> {
        return repo.findAll()
                .map {
                    val output = TreatmentIdTitleDto(it.id)

                    titleRepo.findByTreatment(it).forEach {
                        output.titles.put(it.language!!, it.field!!)
                    }

                    output
                }
    }

    fun delete(id: Long){
        return treatmentCrud.delete(repo.getOne(id))
    }

    fun getHtml(id : Long) : String {
        return bodyRepo.findByTreatmentAndLanguage(repo.getOne(id), LanguageContext.getLanguage()).orElse(null)?.html ?: "";
    }

    fun rawTreatment(id : Long) : Treatment {

        val output = treatmentCrud.getById(id)!!
        output.titles = hibernateUnproxy(output.titles!!)
        output.bodies = hibernateUnproxy(output.bodies!!)
        return output
    }
}