package xyz.nzt48.covidnews.services.abstractions

import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.util.Assert
import xyz.nzt48.covidnews.models.base.Base
import java.util.*

open class Crud <T : Base, Repo : JpaRepository<T, Long>>
        (var repo : Repo) {


    fun all() : List<T>{
        return repo.findAll();
    }

    fun paged(pageable : Pageable) : Page<T> {
        return repo.findAll(pageable);
    }

    fun examplePaged(example : T?, pageable: Pageable) : Page<T> {
        if (example != null) {
            return repo.findAll(Example.of(example), pageable);
        }
        return this.paged(pageable);
    }

    fun getById(id : Long) : T? {
        return repo.getOne(id);
    }

    fun delete(obj : T) {
        repo.delete(obj);
    }

    fun save(obj : T) : T{

        if(obj.id != null){
            Assert.isTrue(getById(obj.id!!) != null, "Bad id")
            obj.updatedAt = Date()
        }
        else {
            obj.createdAt = Date()
        }

        return repo.save(obj);
    }
}