package xyz.nzt48.covidnews.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import xyz.nzt48.covidnews.models.reports.Article
import xyz.nzt48.covidnews.repos.ArticleRepo
import xyz.nzt48.covidnews.services.abstractions.Crud

@Service
class ArticleService  : Crud <Article, ArticleRepo> {

    @Autowired
    constructor(repo : ArticleRepo) : super(repo){}



}