package xyz.nzt48.covidnews.enums

enum class Emotion {
    POSITIVE,
    NEUTRAL,
    NEGATIVE;
}