package xyz.nzt48.covidnews.enums

enum class Purpose {
    GOOD_NEWS,
    STATUS_UPDATE,
    STUDY;
}