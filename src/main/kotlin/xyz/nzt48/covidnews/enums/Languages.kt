package xyz.nzt48.covidnews.enums

enum class Languages (private val value : String) {
    EN("en"),
    EN_US("en-US"),
    PT("pt"),
    PT_BR("pt-BR");

    fun getLanguageCode () : String {
        return value;
    }
}