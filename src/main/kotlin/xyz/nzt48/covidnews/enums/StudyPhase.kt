package xyz.nzt48.covidnews.enums

enum class StudyPhase {
    PRE_CLINICAL,
    PHASE_1,
    PHASE_2,
    PHASE_3,
    PHASE_4,
    COMMERCIALIZED;
}