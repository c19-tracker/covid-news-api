package xyz.nzt48.covidnews.utils

import org.hibernate.Hibernate
import org.hibernate.proxy.HibernateProxy



@Suppress("UNCHECKED_CAST")
fun <T> hibernateUnproxy (proxied : T) : T{
    var output = proxied
    if(output is HibernateProxy){
        Hibernate.initialize(output)
        output = (output as HibernateProxy)
                .hibernateLazyInitializer
                .implementation as T
    }
    return output
}