package xyz.nzt48.covidnews.dto

import xyz.nzt48.covidnews.enums.Languages

class TreatmentIdTitleDto(val id: Long?){
    val titles = HashMap<Languages, String>()
}