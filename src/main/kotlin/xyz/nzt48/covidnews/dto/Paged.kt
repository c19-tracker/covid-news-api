package xyz.nzt48.covidnews.dto

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

class Paged (var page : Int,
             var size : Int,
             var sortColumn : String?,
             var sortOrder : Sort.Direction?) {

    fun toPageable() : PageRequest {
        if(sortColumn != null && sortOrder != null){
            return PageRequest.of(page, size, Sort.by(sortOrder!!, sortColumn))
        }
        return PageRequest.of(page, size);
    }

}