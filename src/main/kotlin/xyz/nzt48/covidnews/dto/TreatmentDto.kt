package xyz.nzt48.covidnews.dto

import xyz.nzt48.covidnews.enums.Languages
import xyz.nzt48.covidnews.enums.StudyPhase

class TreatmentDto {
    var id : Long? = null;
    var language : Languages? = null;
    var title : String? = null;
    var body : String? = null;
    var phase : StudyPhase? = null;
}