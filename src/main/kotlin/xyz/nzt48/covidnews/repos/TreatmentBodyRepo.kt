package xyz.nzt48.covidnews.repos

import org.springframework.data.jpa.repository.JpaRepository
import xyz.nzt48.covidnews.enums.Languages
import xyz.nzt48.covidnews.models.treatments.Treatment
import xyz.nzt48.covidnews.models.treatments.TreatmentBody
import java.util.*

interface TreatmentBodyRepo : JpaRepository<TreatmentBody, Long> {
    fun findByTreatmentAndLanguage(treatment : Treatment, language : Languages) : Optional<TreatmentBody>;
    fun findByTreatment(treatment : Treatment) : List<TreatmentBody>
}