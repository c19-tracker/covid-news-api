package xyz.nzt48.covidnews.repos

import org.springframework.data.jpa.repository.JpaRepository
import xyz.nzt48.covidnews.enums.Languages
import xyz.nzt48.covidnews.models.treatments.Treatment
import xyz.nzt48.covidnews.models.treatments.TreatmentTitle
import java.util.*

interface TreatmentTitleRepo : JpaRepository<TreatmentTitle, Long> {
    fun findByTreatmentAndLanguage(treatment : Treatment, language : Languages) : Optional<TreatmentTitle>
    fun findByTreatment(treatment : Treatment) : List<TreatmentTitle>

}