package xyz.nzt48.covidnews.repos

import org.springframework.data.jpa.repository.JpaRepository
import xyz.nzt48.covidnews.models.reports.Article

interface ArticleRepo : JpaRepository<Article, Long> {
}